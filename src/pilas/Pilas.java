/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilas;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Rodrigo Balan
 */
public class Pilas {

    public static void main(String[] args) {

        System.out.println("Rodrigo Manuel Gaytan Balan-63842");
        System.out.println("Ejercio 1");
        Pilas.Pila1();
        System.out.println("-------");
        System.out.println("Ejercicio 2");
        Pilas.Pila2();
        System.out.println("-------");
        Scanner entrada = new Scanner(System.in);
        String cadena = "";
        String mensaje = "";
        System.out.println("Ejercicio 3");
        System.out.println("Introduzca un mensaje");

        while (mensaje.equalsIgnoreCase("")) {
            cadena = entrada.nextLine();
            String resultado = "";
            char[] array = {'a', 'e', 'i', 'o', 'u'};
            for (int i = 0; i < array.length; i++) {
                switch (Pila3(cadena, array[i])) {

                    case 0:
                        resultado = resultado + "La letra " + array[i] + " no se encuentra. ---- ";
                        break;

                    case -1:
                        resultado = resultado + "El número de veces que la letra " + array[i] + "  se repite es impar. ---- ";
                        break;
                    case 1:

                        resultado = resultado + "El número de veces que la letra " + array[i] + " se repite es par. ---- ";
                        break;

                }

            }

            System.out.println(resultado);
        }

    }

    public static void Pila1() {
        String cadenaA = "(123*4)+((12-1)*(1+2))";
        Stack pila = new Stack();
        boolean flag = true;
        char[] cad = cadenaA.toCharArray();
        for (char ca : cad) {
            if (ca == '(') {
                pila.push(ca);
                System.out.println("Se esta insertando = " + ca);
            }
            if (ca == ')') {
                if (!pila.empty()) {
                    pila.pop();
                    System.out.println("Se esta extrayendo = " + ca);
                } else {
                    flag = false;
                }
            }
        }
        System.out.println(pila.size());
        System.out.println(flag);
        if (pila.size() == 0 && flag) {
            System.out.println("La expresión es correcta");
        } else {
            System.out.println("La expresión es incorrecta");
        }

    }

    public static void Pila2() {
        String cadena = "<b><i>Hola ISC</i></b";
        Stack pila = new Stack();
        boolean flag = true;
        char[] cad = cadena.toCharArray();
        for (char ca : cad) {
            if (ca == '<') {
                pila.push(ca);
                System.out.println("Se esta insertando = " + ca);
            }
            if (ca == '>') {
                if (!pila.empty()) {
                    pila.pop();
                    System.out.println("Se esta extrayendo = " + ca);
                } else {
                    flag = false;
                }
            }
        }
        System.out.println(pila.size());
        System.out.println(flag);
        if (pila.size() == 0 && flag) {
            System.out.println("La expresión es correcta");
        } else {
            System.out.println("La expresión es incorrecta");
        }

    }

    public static int Pila3(String cadena, char vocal) {
        Stack<String> pila = new Stack<String>();
        char v = vocal;
        String vocalA = String.valueOf(vocal);
        int a = 0;
        int aa = 0;
        while (a < cadena.length()) {
            if (Character.toLowerCase(cadena.charAt(a)) == v && pila.empty()) {
                pila.push(vocalA);
                aa++;
            } else if (Character.toLowerCase(cadena.charAt(a)) == v && !pila.empty()) {
                pila.pop();
            }
            a++;
        }
        if (aa == 0) {
            return 0;
        } else {
            if (pila.empty()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

}
